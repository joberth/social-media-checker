(function ()
{

  var CONFIG = null;

  function loadConfig()
  {
    var request = new XMLHttpRequest();
    request.overrideMimeType('application/json');
    request.open('GET', 'smc-config.json', true);
    request.onreadystatechange = function ()
    {
      if (request.readyState == 4 && request.status == "200")
      {
        CONFIG = JSON.parse(request.responseText);
        init();
      }
    };
    request.send(null);
  }

  function init()
  {
    debug('loaded social-media-checker (v' + CONFIG.version + ')');

    new Smc().run();
  }

  var Smc = function ()
  {
    // load google analytics
    if (CONFIG.googleanalytics.active)
    {

    }

    this.o = {};

    this.done = function ()
    {
      debug(this.o);
      debug(this.o, 2);

      if (CONFIG.googleanalytics.active)
      {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', CONFIG.googleanalytics.trackingId, 'auto');
        ga('send', 'pageview');

        for (var i in this.o)
          if (this.o.hasOwnProperty(i))
          {
            ga('send', {
              hitType:       'event',
              eventCategory: 'Social Media Usage',
              eventAction:   i,
              eventLabel: this.o[i] ? 'yes' : 'no'
            });
          }
      }
    };

    this.needed = null;

    this.run = function ()
    {
      for (var i in this.checker)
        if (this.checker.hasOwnProperty(i))
        {
          if (CONFIG.checker[i].active)
          {
            (function (obj, i)
            {
              obj.checker[i](
                function (status)
                {
                  obj.o[i] = status;
                  if (Object.keys(obj.o).length == obj.needed) obj.done();
                }
              );
            })(this, i);

            this.needed = this.needed === null ? 1 : this.needed + 1;
          }
        }
    };
  };

  Smc.prototype.checker =
    {
      google:     function (callback)
                  {
                    var img     = new Image();
                    img.onload  = function ()
                    {
                      callback(true);
                    };
                    img.onerror = function ()
                    {
                      callback(false);
                    };
                    img.src     = CONFIG.checker.google.url;
                  },
      googleplus: function (callback)
                  {
                    var img     = new Image();
                    img.onload  = function ()
                    {
                      callback(true);
                    };
                    img.onerror = function ()
                    {
                      callback(false);
                    };
                    img.src     = CONFIG.checker.googleplus.url;
                  },
      twitter:    function (callback)
                  {
                    var img     = new Image();
                    img.onload  = function ()
                    {
                      callback(true);
                    };
                    img.onerror = function ()
                    {
                      callback(false);
                    };
                    img.src     = CONFIG.checker.twitter.url;
                  },
      facebook:   function (callback)
                  {
                    window.fbAsyncInit = function ()
                    {
                      FB.init({
                        appId      : CONFIG.checker.facebook.appId,
                        version    : 'v2.8',
                        status: true, cookie: true, xfbml: true
                      });
                      FB.getLoginStatus(
                        function (response)
                        {
                          if (response.status != "unknown") callback(true);
                          else callback(false);
                        }
                      );
                    };

                    (function(d, s, id){
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) {return;}
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/sdk.js";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                  }
    };

  function debug(msg, type)
  {
    if (CONFIG.debug)
    {
      switch (type)
      {
        case 0:
          console.error(msg);
          break;
        case 1:
          console.log(msg);
          break;
        case 2:
          console.table(msg);
          break;
        default:
          console.info(msg);
          break;
      }
    }
  }

  loadConfig();

})();