**Example**

Open Console to view debug output:
http://smc.johannesberthel.de/example.html

**Usage**

Add following code to your page:
```
<script src="smc.js"></script>
```

Copy `smc-config.json.example` to `smc-config.json`.

Add your _Google Analytics TrackingID_ and _Facebook API Key_ to the smc-config.json.

Toggle the `active` property in the `smc-config.json` to disable a function.

